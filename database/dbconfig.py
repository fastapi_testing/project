import os
from dotenv import load_dotenv
load_dotenv()

DB_HOST = "pgdb"
DB_NAME = 'postgres'
DB_PORT = '5432'
DB_USER = 'postgres'
DB_PASSWORD = 'postgres'


def asyncpg_settings():
    return f"postgresql+asyncpg://postgres:postgres@pgdb:5432/postgres"
