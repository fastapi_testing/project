from database.dbtable import session
from database.dbtable import Worker
from sqlalchemy import select


async def salary_get(token):
    async with session() as sess:
        query = select(Worker.salary, Worker.promdate).where(Worker.token == token)
        execut = await sess.execute(query)
        values = execut.fetchall()
        return {"salary": int(values[0][0]), "promdate": str(values[0][1])}
