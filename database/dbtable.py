from sqlalchemy import Null, String, DateTime, pool
from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker
from sqlalchemy.orm import DeclarativeBase, mapped_column, Mapped
from .dbconfig import asyncpg_settings


engine = create_async_engine(url=asyncpg_settings(), echo=True, poolclass=pool.NullPool)


session = async_sessionmaker(engine, expire_on_commit=False)


class Model(DeclarativeBase):
    pass


class Worker(Model):
    __tablename__ = "worker"
    id: Mapped[int] = mapped_column(primary_key=True)
    login: Mapped[str] = mapped_column(String,unique=True)
    password: Mapped[str] = mapped_column(String)
    token: Mapped[str] = mapped_column(String, nullable=True)
    salary: Mapped[int] = mapped_column(String)
    promdate = mapped_column(DateTime, default=Null, nullable=True)


async def create_table():
    async with engine.begin() as conn:
        await conn.run_sync(Model.metadata.create_all)


async def drop_table():
    async with engine.begin() as conn:
        await conn.run_sync(Model.metadata.drop_all)
