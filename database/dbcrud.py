from pydantic import TypeAdapter
from sqlalchemy import select
from database.dbtable import session
from database.dbtable import Worker
from jwt_token.jwt_tok import token_create


async def insert_worker(model):
    async with session() as sess:
        worker = Worker(login=model.login, password=model.password,
                        salary=str(model.salary), promdate=model.promdate)
        aboba = await token_create(model)
        worker.token = aboba
        sess.add(worker)
        await sess.commit()
        await sess.refresh(worker)
        return worker.id


async def drop_worker(id):
    async with session() as sess:
        query = select(Worker).where(Worker.id == id)
        worker = await sess.execute(query)
        await sess.delete(worker.fetchone()[0])
        await sess.commit()




