from fastapi import HTTPException

from database.dbtable import session
from database.dbtable import Worker
from sqlalchemy import select

async def authenticate(model):
    async with session() as sess:
        try:
            query = select(Worker.token).where(model.login == Worker.login, model.password == Worker.password)
            execut = await sess.execute(query)
            return {"secret_token": execut.fetchone()[0]}
        except:
            raise HTTPException(status_code=404,detail="Token doesnt exist")