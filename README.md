# ТЕСТВОЕ ЗАДАНИЕ
### В решении использовался docker,поэтому для запуска сервиса необходимо передать команду в терминал
```
git clone https://gitlab.com/fastapi_testing/project
```
### В папке необходимо прописать  
```
docker compose up 
```
### А затем
```
docker compose exec fastapi python main.py
```
### Сервис готов к работе,далее будут указанны эндпоинты и их методы
1. "/worker", принимает post-запросы, с передавыемыми данными(login,password,promdate,salary) в формате json, создает объект базы данных из полученнего запроса
2. "/auth", принимает post-запросы, с передаваемыми данными(login,password) в формате json,возвращает "secret-key" если передаваемые данные есть в модели в базе данных
3. "/salary", принимает get-запросы , проверяет наличие ("secret-key") значения в заголовках запроса, и если таковой есть, выполняет проверку подлинности, по выполнению которой возвращает информацию о зар.плате и повышении сотрудника

## Также к заданию были написанны тесты, находящиеся в папке "/tests" , запуск которых выполняется командой 
```
docker compose fastapi pytest tests/
```