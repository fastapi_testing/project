import asyncio

from fastapi import FastAPI
from tests.testschema import Test_model
from database.dbtable import create_table,drop_table
from database.dbcrud import insert_worker
from endpoints.workercrud import router as router_oper
from endpoints.salary import router as router_salary
from endpoints.worker_auth import router as router_auth

app = FastAPI()

app.include_router(router_oper)
app.include_router(router_salary)
app.include_router(router_auth)

if __name__ == "__main__":
    async def start_function():
        try:
            await drop_table()
            await create_table()
            await insert_worker(model=Test_model)
        except:
            print("exception",Exception)
    asyncio.run(start_function())

