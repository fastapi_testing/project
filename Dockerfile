FROM python:latest

ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1

RUN mkdir "/tz"

WORKDIR /tz

COPY . /tz

EXPOSE 8000

RUN pip install -r requirements.txt

