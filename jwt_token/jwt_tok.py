import jwt
import datetime

secret_key = "testapp"


async def token_create(model):
    payload = {"login": model.login, "password": model.password,
               "exp_time": str(model.promdate + datetime.timedelta(days=5))}
    token = jwt.encode(payload=payload, key=secret_key)
    return token


async def is_valid_token(token):
    try:
        tok = jwt.decode(token, key=secret_key, algorithms=['HS256'])
        data = tok.get("exp_time")
        date = datetime.datetime.strptime(data, "%Y-%m-%d %H:%M:%S")
        if datetime.datetime.now() > date:
            return "Token doesnt exist or expired"
        else:
            return True
    except:
        return "Token doesnt exist or expired"
