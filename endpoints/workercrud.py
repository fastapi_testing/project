import datetime
from fastapi import APIRouter
from database.dbcrud import insert_worker
from endpoints.worker_auth import Auth

router = APIRouter(
    prefix="/worker"
)


class Worker(Auth):
    salary: int
    promdate: datetime.datetime


@router.post("")
async def worker_create(model: Worker):
    id = await insert_worker(model)
    return {"model": model,"id": id}

