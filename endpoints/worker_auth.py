from pydantic import BaseModel, TypeAdapter
from fastapi import APIRouter
from database.auth import authenticate


router = APIRouter(
    prefix="/auth"
)


class Auth(BaseModel):
    login: str
    password: str


@router.post("")
async def work_auth(model: Auth):
    auth = await authenticate(model)
    return auth
