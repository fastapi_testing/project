from database.salary import salary_get
from fastapi import APIRouter, Header, HTTPException
from jwt_token.jwt_tok import is_valid_token


router = APIRouter(
    prefix="/salary"
)


@router.get("")
async def work_auth_get(secret_key: str = Header()):
    try:
        check = await is_valid_token(secret_key)
        if check:
            salary_info = await salary_get(secret_key)
            return {"salary_information": salary_info}
        else:
            raise HTTPException(status_code=401, detail="Token may not exist or expired")
    except:
        raise HTTPException(status_code=401, detail="Token may not exist or expired")
