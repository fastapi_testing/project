import datetime
from pydantic import BaseModel
from database.auth import authenticate


class Auth(BaseModel):
    login: str
    password: str


class Worker(Auth):
    salary: int
    promdate: datetime.datetime


Test_model = Worker(login='Test',password='Test',salary=12345,promdate="2024-12-12 01:01:01")

async def return_token():
    return await authenticate(Test_model)