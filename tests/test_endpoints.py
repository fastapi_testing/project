from fastapi.testclient import TestClient
import pytest
import asyncio
from database.dbcrud import drop_worker
from main import app
from .testschema import return_token


client = TestClient(app)


@pytest.mark.asyncio
async def test_get_salary():
    token = await return_token()
    good_response = client.get("/salary", headers={"secret-key": token.get("secret_token")})
    wrong_response = client.get("/salary", headers={"secret-key": "eyjahkxoleoeqlsaasa"})
    headersless_response = client.get("/salary")
    wrong_request_post = client.post("/salary")
    wrong_request_put = client.put("/salary")
    wrong_request_patch = client.patch("/salary")

    assert wrong_request_post.status_code == 405
    assert wrong_request_put.status_code == 405
    assert wrong_request_patch.status_code == 405
    assert headersless_response.status_code == 422
    assert wrong_response.status_code == 401
    assert good_response.status_code == 200


@pytest.mark.asyncio
async def test_worker_create_test():
    good_response_model = client.post("/worker", json={
        "login": "Testuser",
        "password": "Testuser",
        "salary": "545454",
        "promdate": "2024-12-12 04:04:04"
    })
    if good_response_model.status_code == 200:
        await drop_worker(good_response_model.json().get("id"))
        assert good_response_model.status_code == 200
    wrong_response_model_data = client.post("/worker", json={
        "login": "Testuser",
        "password": "Testuser",
        "salary": "545454",
        "promdate": "2024-123-12 04:04:04"
    })
    assert wrong_response_model_data.status_code == 422

    wrong_response_model_salary = client.post("/worker", json={
        "login": "Testuser",
        "password": "Testuser",
        "salary": "545454",
        "promdate": "2024-123-12 04:04:04"
    })
    assert wrong_response_model_salary.status_code == 422

    wrong_response_post = client.post("/worker")
    wrong_response_put = client.put("/worker")
    wrong_response_patch = client.patch("/worker")
    wrong_response_get = client.get("/worker")

    assert wrong_response_post.status_code == 422
    assert wrong_response_put.status_code == 405
    assert wrong_response_patch.status_code == 405
    assert wrong_response_get.status_code == 405

def test_existed_model():
    try:
        wrong_response_model_existed = client.post("/worker", json={
            "login": "Test",
            "password": "Test",
            "salary": "545454",
            "promdate": "2024-12-12 04:04:04"
        })
    except:
        assert True == True


@pytest.mark.asyncio
async def test_workerk_auth():
    good_response = client.post("/auth", json={
        "login": "Test",
        "password": "Test"
    })
    assert good_response.status_code == 200
    wrong_response = client.post("/auth", json={
        "login": "Wrong",
        "password": "Test"
    })
    assert wrong_response.status_code == 404

    wrong_response_post = client.post("/auth")
    wrong_response_put = client.put("/auth")
    wrong_response_patch = client.patch("/auth")
    wrong_response_get = client.get("/auth")

    assert wrong_response_post.status_code == 422
    assert wrong_response_put.status_code == 405
    assert wrong_response_get.status_code == 405
    assert wrong_response_patch.status_code == 405

